<?php
$cdir = scandir('DataAvatar');
$cdir = array_filter($cdir, function ($value) {
    return !in_array($value,array(".",".."));
});
$path = 'DataAvatar/' . $cdir[array_rand($cdir)];
$file_extension = strtolower(substr(strrchr($path,"."),1));
switch( $file_extension ) {
    case "gif": $ctype="image/gif"; break;
    case "png": $ctype="image/png"; break;
    case "jpeg":
    case "jpg": $ctype="image/jpeg"; break;
    case "svg": $ctype="image/svg+xml"; break;
    default:
}
header('Content-type: ' . $ctype);
echo file_get_contents($path);